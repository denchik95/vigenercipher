﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VigenereCipher
{
    public static class VigenereCipherEncryption
    {
        private static string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

        public static string Decrypt(string text, string key)
        {

            string result = "";
            int keyIndex = 0;

            foreach (char currentChar in text)
            {
                if (!alphabet.Contains(currentChar))
                {
                    result += currentChar;
                }
                else
                {
                    int newCharIndex = (alphabet.IndexOf(currentChar) + alphabet.Length -
                                        alphabet.IndexOf(key[keyIndex])) % alphabet.Length;
                    result += alphabet[newCharIndex];

                    keyIndex = (keyIndex + 1) % key.Length;
                }
            }

            return result;
        }

        public static string Encrypt(string text, string key)
        {
            string result = "";
            int keyIndex = 0;

            foreach (char currentChar in text)
            {
                if (!alphabet.Contains(currentChar))
                {
                    result += currentChar;
                }
                else
                {
                    int newCharIndex = (alphabet.IndexOf(currentChar) +
                                        alphabet.IndexOf(key[keyIndex])) % alphabet.Length;
                    result += alphabet[newCharIndex];

                    keyIndex = (keyIndex + 1) % key.Length;
                }
            }

            return result;
        }
    }
}
