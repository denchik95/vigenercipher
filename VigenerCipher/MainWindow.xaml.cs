﻿using System;
using System.Collections.Generic;
using System.Collections;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace VigenereCipher
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void EncryptButton_Click(object sender, RoutedEventArgs e)
        {
            string key = textBoxKey.Text;
            string text = textBoxInputText.Text;
            try
            {
                string result = VigenereCipherEncryption.Encrypt(text, key);
                textBoxOutputText.Text = result;
            }
            catch
            {
                MessageBox.Show("Не введен ключ", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }


        private void DecryptButton_Click(object sender, RoutedEventArgs e)
        {
            string key = textBoxKey.Text;
            string text = textBoxInputText.Text;

            try
            {
                string result = VigenereCipherEncryption.Decrypt(text, key);
                textBoxOutputText.Text = result;
            }
            catch
            {
                MessageBox.Show("Не введен ключ", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }


        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog()
            {
                DefaultExt = "txt",
                Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*"
            };
            bool? result = saveFileDialog.ShowDialog(this);
            if (result != true) return;
            string fileName = saveFileDialog.FileName;
            File.WriteAllText(fileName, textBoxOutputText.Text);
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                Filter = "Text files (*.txt)|*.txt"
            };
            bool? result = openFileDialog.ShowDialog(this);
            if (result != true) return;
            string fileName = openFileDialog.FileName;
            if (!fileName.EndsWith(".txt"))
            {
                MessageBox.Show("Формат не поддерживается", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string text = File.ReadAllText(fileName, Encoding.GetEncoding(1251));
            textBoxInputText.Text = text;

        }
        private void Readme_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Программа выполняет функции шифровки / дешифровки шифром Виженера.\n" +
                "Имеет следующие возможности:\n" +
                "1.Зашифровать текст\n" +
                "2.Расшифровать текст\n" +
                "3.Загрузить  текст из файла\n" +
                "4.Сохранить результат в файл\n" +
                "Инструкция:\n" +
                "1.Ввести ключ\n" +
                "2.Ввести текст или загрузить из файла(формат.txt)\n" +
                "3.Нажать кнопку зашифровать или расшифровать\n" +
                "4.Сохранить результат в файл(при необходимости)",
                "Readme", MessageBoxButton.OK, MessageBoxImage.Information);
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            WindowOne.Close();
        }
    }
}
